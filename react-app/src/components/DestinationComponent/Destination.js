import React, { useEffect, useState } from "react";
import { Link, useParams, Navigate } from "react-router-dom";

import destinations from "../../database/destinations.json";

import "./Destination.css";

const getLiked = (current, sourceArray, neededElements) => {
  var result = [];
  let i = 0;
  while (i < neededElements) {
    const random = Math.floor(Math.random()*sourceArray.length);
    if (!result.includes(sourceArray[random]) && sourceArray[random] !== current) {
      result.push(sourceArray[random]);
      i++;
    }
  }
  return result;
}

const Destination = () => {
  const { id } = useParams();
  const [bigPicture, setBigPicture] = useState({
    picture: '',
    open: false,
    alt: ''
  });
  let dest = destinations.filter(destination => destination.id === Number(id))[0];

  const liked = getLiked(dest, destinations, 3);

  useEffect(() => {
    if (dest) {
      document.title = `${ dest.title } | Travel Agency`;
    }
  });

  if (!dest) {
    return (<Navigate to="/destinations" />)
  }

  return (
    <div id="destination">
      <section className="holiday-section-1">
        <section className="holiday__resume">
          <h2 className="holiday__resume--city">{ dest.title }</h2>
          <span className="holiday__resume--company">{ dest.hotel }</span>
          <span className="holiday__resume--star">&#9733;&#9733;&#9733;</span>
          <br />
          <span className="holiday__resume--duration">{ dest.duration }</span>
          <p className="holiday__resume--short-description">{ dest.desc }</p>
          <button type="button" className="holiday__resume--choose">Choose a date</button>
          <span className="holiday__resume-price">{ dest.price }€</span>
        </section>
        <section className="holiday__gallery">
          { dest.pictures.map((picture, index) => {
            return (
              <div
                key={ index }
                className="holiday__gallery-image"
                onClick={() =>
                  setBigPicture({
                    picture: picture,
                    open: true,
                    alt: ` View of ${dest.title}`
                  }) }>
                <img src={`https://picsum.photos/id/${picture}/1200/800.webp`} alt={ ` View of ${dest.title}` } />
                <div className="holiday__gallery-overlay ">
                  <div className="holiday__gallery-zoom"></div>
                </div>
              </div>
            );
          }) }
          <div className={bigPicture.open ? "big-picture" : "hide" } onClick={ () => setBigPicture({ picture: '', open: false, alt: '' }) }>
            <div className="content">
              <p className="exit">X - Exit</p>
              <figure>
                <img
                  src={`https://picsum.photos/id/${bigPicture.picture}/1200/800.webp`}
                  alt={bigPicture.alt}
                />
              </figure>
              <p className="desc">{bigPicture.alt}</p>
            </div>
          </div>
        </section>
      </section>
      <section className="banner banner--medium"
        style={{ backgroundImage: `url('https://picsum.photos/id/${dest.pictures[dest.pictures.length - 1]}/1200/800.webp')` }}>
        <div className="banner__wrapper">
          <div>
            <h2>{ dest.title }</h2>
            <p>{ dest.subtitle }</p>
          </div>
        </div>
      </section>
      <section className="holiday-section-2" dangerouslySetInnerHTML={{__html: dest.content}}></section>
      <section className="home-section-2 holiday-section-3">
        <h2>You may also like</h2>
        <div className="destinations">
          { liked.map((destination) => {
            return (
              <div key={ destination.id } className="destination">
                <div className="destination__background" style={{ backgroundImage: `url('https://picsum.photos/id/${destination.pictures[0]}/1200/800.webp')` }}>
                  <Link to={`/destinations/${destination.id}`}>
                    <div className="destination__content">
                      <h3 className="destination__city">{ destination.title }</h3>
                      <div className="destination__content-bottom">
                        <span className="destination__info">{ destination.hotel }</span>
                        <span className="destination__duration">{ destination.duration }</span>
                      </div>
                      <span className="destination__content-price">{ destination.price }€</span>
                    </div>
                  </Link>
                </div>
              </div>
            );
          }) }
        </div>
      </section>
    </div>
  );
}

export default Destination;
