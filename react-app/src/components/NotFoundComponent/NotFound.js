import React from "react";

// Image
import NotFoundImg from "../../images/not-found.svg"

import "./NotFound.css";

class NotFound extends React.Component {
  render() {
    document.title = `Page not found | Travel Agency`;
    return (
      <section className="not-found">
        <figure>
          <img src={ NotFoundImg } alt="Page not found" />
        </figure>
        <h2>Hmm, it seems that this page does not exist anymore...</h2>
        <p>
          We are sorry for the inconvenience, but let us take you to your next
          destination!
        </p>
      </section>
    );
  }
}

export default NotFound;
