import React from "react";
import { Link } from "react-router-dom";

class Destinations extends React.Component {
  render() {
    return (
      <div id="destinations">

        <section className="banner" style={{ backgroundImage: "url('https://picsum.photos/1200/800?image=572')" }}>
          <div className="banner__wrapper">
            <div>
              <h2>Our deals</h2>
              <p>Choose your dream vacation in our list of destinations</p>
            </div>
          </div>
        </section>
        <section className="destinations-section-1">
          <div className="destination">
            <div className="destination__banner" style={{ backgroundImage: "url('https://picsum.photos/1200/800?image=896')" }}>
              <div>
                <h3>France</h3>
              </div>
            </div>
            <div className="destination__city-list">
              <div>
                <ul>
                  <li><Link to="/">Paris</Link></li>
                  <li><Link to="/">Toulouse</Link></li>
                  <li><Link to="/">Bordeaux</Link></li>
                  <li><Link to="/">Montpellier</Link></li>
                  <li><Link to="/">La Rochelle</Link></li>
                </ul>
              </div>
              <div>
                <ul>
                  <li><Link to="/">Marseille</Link></li>
                  <li><Link to="/">Nice</Link></li>
                  <li><Link to="/">Côte d'Azur</Link></li>
                  <li><Link to="/">La Ciotat</Link></li>
                  <li><Link to="/">Cannes</Link></li>
                </ul>
              </div>
              <Link to="/" className="destination__see-more">See more</Link>
            </div>
          </div>
          <div className="destination">
            <div className="destination__banner" style={{ backgroundImage: "url('https://picsum.photos/1200/800?image=993')" }}>
              <div>
                <h3>France</h3>
              </div>
            </div>
            <div className="destination__city-list">
              <div>
                <ul>
                  <li><Link to="/">Paris</Link></li>
                  <li><Link to="/">Toulouse</Link></li>
                  <li><Link to="/">Bordeaux</Link></li>
                  <li><Link to="/">Montpellier</Link></li>
                  <li><Link to="/">La Rochelle</Link></li>
                </ul>
              </div>
              <div>
                <ul>
                  <li><Link to="/">Marseille</Link></li>
                  <li><Link to="/">Nice</Link></li>
                  <li><Link to="/">Côte d'Azur</Link></li>
                  <li><Link to="/">La Ciotat</Link></li>
                  <li><Link to="/">Cannes</Link></li>
                </ul>
              </div>
              <Link to="/" className="destination__see-more">See more</Link>
            </div>
          </div>
          <div className="destination">
            <div className="destination__banner" style={{ backgroundImage: "url('https://picsum.photos/1200/800?image=990')" }}>
              <div>
                <h3>France</h3>
              </div>
            </div>
            <div className="destination__city-list">
              <div>
                <ul>
                  <li><Link to="/">Paris</Link></li>
                  <li><Link to="/">Toulouse</Link></li>
                  <li><Link to="/">Bordeaux</Link></li>
                  <li><Link to="/">Montpellier</Link></li>
                  <li><Link to="/">La Rochelle</Link></li>
                </ul>
              </div>
              <div>
                <ul>
                  <li><Link to="/">Marseille</Link></li>
                  <li><Link to="/">Nice</Link></li>
                  <li><Link to="/">Côte d'Azur</Link></li>
                  <li><Link to="/">La Ciotat</Link></li>
                  <li><Link to="/">Cannes</Link></li>
                </ul>
              </div>
              <Link to="/" className="destination__see-more">See more</Link>
            </div>
          </div>
          <div className="destination">
            <div className="destination__banner" style={{ backgroundImage: "url('https://picsum.photos/1200/800?image=990')" }}>
              <div>
                <h3>France</h3>
              </div>
            </div>
            <div className="destination__city-list">
              <div>
                <ul>
                  <li><Link to="/">Paris</Link></li>
                  <li><Link to="/">Toulouse</Link></li>
                  <li><Link to="/">Bordeaux</Link></li>
                  <li><Link to="/">Montpellier</Link></li>
                  <li><Link to="/">La Rochelle</Link></li>
                </ul>
              </div>
              <div>
                <ul>
                  <li><Link to="/">Marseille</Link></li>
                  <li><Link to="/">Nice</Link></li>
                  <li><Link to="/">Côte d'Azur</Link></li>
                  <li><Link to="/">La Ciotat</Link></li>
                  <li><Link to="/">Cannes</Link></li>
                </ul>
              </div>
              <Link to="/" className="destination__see-more">See more</Link>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Destinations;
