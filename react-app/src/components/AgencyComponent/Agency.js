import React from "react";

import agency from "../../database/agency.json";

class Agency extends React.Component {
  render() {
    document.title = `Agency | Travel Agency`;
    return (
      <div id="agency">
        <section className="banner" style={{ backgroundImage: "url('https://picsum.photos/1200/800?image=192')" }}>
          <div className="banner__wrapper">
            <div>
              <h2>Our Agency</h2>
              <p>Here is a list of our agency world wide</p>
            </div>
          </div>
        </section>
        <section className="destinations-section-1">
          { agency.map((agency) => {
            return (
              <div key={ agency.id } className="destination">
                <div className="destination__banner" style={{ backgroundImage: `url('https://picsum.photos/id/${agency.picture}/1200/800.webp')` }}>
                  <div>
                    <h3>{ agency.name }</h3>
                  </div>
                </div>
                <div className="destination__city-list">
                  { agency.address }
                  <br />
                  { agency.zip }
                  <br />
                  { agency.country }
                  <br />
                  { agency.phone }
                  <br />
                  <a href={`mailto:${ agency.email }`}>{ agency.email }</a>
                </div>
              </div>
            );
          }) }
        </section>
      </div>
    );
  }
}

export default Agency;
