import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";

// Pages
import Home from "./HomeComponent/Home";
import Destinations from "./DestinationsComponent/Destinations";
import Destination from "./DestinationComponent/Destination";
import Deals from "./DealsComponent/Deals";
import Agency from "./AgencyComponent/Agency";
import Contact from "./ContactComponent/Contact";
import Terms from "./TermsComponent/Terms";
import NotFound from "./NotFoundComponent/NotFound";

// Models
import Header from "../models/HeaderModel/Header";
import Footer from "../models/FooterModel/Footer";

class App extends React.Component {

  componentDidUpdate(prevProps) {
		if (this.props.location !== prevProps.location) {
			window.scrollTo(0, 0);
		}
	}
  
  render() {
    return (
      <main>
        <Header />
        <Routes>
          <Route path="/" element={ <Home /> } />
          <Route path="/destinations" element={ <Destinations /> } />
          <Route path="/destinations/:id" element={ <Destination /> } />
          <Route path="/deals" element={ <Deals /> } />
          <Route path="/contact" element={ <Contact /> } />
          <Route path="/agency" element={ <Agency /> } />
          <Route path="/terms" element={ <Terms /> } />
          <Route path="/terms" element={ <Terms /> } />
          <Route path="/404" element={ <NotFound /> } />
          <Route path="*" element={ <Navigate to="/404" /> } />
        </Routes>
        <Footer />
      </main>
    );
  }
}

export default App;
