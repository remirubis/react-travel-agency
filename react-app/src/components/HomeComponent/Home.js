import React from "react";
import { Link } from "react-router-dom";

import destinations from "../../database/destinations.json";

const deal = destinations.filter(destination => destination.dealOfTheDay)[0];

class App extends React.Component {
  render() {
    document.title = `Home | Travel Agency`;
    return (
      <main>
        <section className="home-section-1">
          <section className="search">
            <h2>Find your holiday</h2>
            <form action="#" method="post">
              <div className="search-inputs">
                <div>
                  <span>From :</span>
                  <input type="text" name="from" placeholder="Paris" />
                </div>
                <div>
                  <span>To :</span>
                  <input type="text" name="to" placeholder="Malte" />
                </div>
                <div>
                  <span>Date :</span>
                  <input type="text" name="date" placeholder="04/11/2018" />
                </div>
                <div>
                  <span>Duration :</span>
                  <select name="duration">
                    <option value="3d">3 days</option>
                    <option value="1w">1 week</option>
                    <option value="2w">2 week</option>
                    <option value="3w">3 week</option>
                  </select>
                </div>
                <input type="submit" value="Find !" />
              </div>
            </form>
          </section>
          <section className="top-deal" style={{ backgroundImage: `url('https://picsum.photos/id/${deal.pictures[0]}/1200/800.webp')` }}>
            <Link to={`/destinations/${deal.id}`}>
              <div className="top-deal__content">
                <h2>Deal of the day</h2>
                <div className="top-deal__content-bottom">
                  <span className="top-deal__city">{ deal.title }</span>
                  <span className="top-deal__info">{ deal.hotel }</span>
                  <span className="top-deal__duration">{ deal.duration }</span>
                </div>
                <span className="top-deal__content-price">{ deal.price }€</span>
              </div>
            </Link>
          </section>
        </section>
        <section className="home-section-2">
          <h2>Top destinations</h2>
          <div className="destinations">
            { destinations.map((destination) => {
              if (!destination.dealOfTheDay) {
                return (
                  <div key={ destination.id } className="destination">
                    <div className="destination__background" style={{ backgroundImage: `url('https://picsum.photos/id/${destination.pictures[0]}/1200/800.webp')` }}>
                      <Link to={`/destinations/${destination.id}`}>
                        <div className="destination__content">
                          <h3 className="destination__city">{ destination.title }</h3>
                          <div className="destination__content-bottom">
                            <span className="destination__info">{ destination.hotel }</span>
                            <span className="destination__duration">{ destination.duration }</span>
                          </div>
                          <span className="destination__content-price">{ destination.price }€</span>
                        </div>
                      </Link>
                    </div>
                  </div>
                );
              }
              return false;
            }) }
          </div>
          <div className="destination__see-more">
            <Link to="/destinations"><button type="button">See more</button></Link>
          </div>
        </section>
      </main>
    );
  }
}

export default App;
