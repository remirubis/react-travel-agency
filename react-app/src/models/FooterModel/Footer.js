import React from "react";
import { Link } from "react-router-dom";

// Pictures
import Calculator from "../../images/calculator.png";
import Discount from "../../images/discount.png";
import Support from "../../images/support.png";
import OnlinePayment from "../../images/online-payment.png";

import "./Footer.css";

class Footer extends React.Component {
  render() {
    return (
      <div id="bottom">
        <section className="home-section-3">
          <div className="ecommerce-icons">
            <div className="ecommerce-icon">
              <img src={ Calculator } alt="calculator" />
              <span>Compare prices</span>
            </div>
            <div className="ecommerce-icon">
              <img src={ Discount } alt="discount" />
              <span>Get the best deals</span>
            </div>
            <div className="ecommerce-icon">
              <img src={ Support } alt="support" />
              <span>7/7 support</span>
            </div>
            <div className="ecommerce-icon">
              <img src={ OnlinePayment } alt="online-payment" />
              <span>Secure online payment</span>
            </div>
          </div>
        </section>
        <footer>
          <section>
            <h3>Links</h3>
            <ul>
              <li><Link to="/">Home</Link></li>
              <li><Link to="/destinations">Destinations</Link></li>
              <li><Link to="/deals">Deals</Link></li>
              <li><Link to="/contact">Contact</Link></li>
              <li><Link to="/terms">Terms & Conditions</Link></li>
            </ul>
          </section>
          <section>
            <h3>Address</h3>
            <p>
              15 rue de la paix
              <br />
              75010, Paris
              <br />
              France
              <br />
              <Link to="/agency">Other addresses</Link>
            </p>
          </section>
          <section className="footer__call">
            <h3>Call Us</h3>
            <span>+1 0805-540-801</span>
          </section>
        </footer>
      </div>
    );
  }
}

export default Footer;
