import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

// Pictures
import Plane from "../../images/plane.png";
import Menu from "../../images/menu.png";
import MenuClose from "../../images/menu-close.png";

import "./Header.css";

import destinations from "../../database/destinations.json";

const Header = () => {
  const [menu, setMenu] = useState(false);

  const changeMenu = () => {
    if (document.body.scrollWidth> 992) {
      setMenu(false);
    }
  }

  useEffect(() => {
    window.addEventListener('resize', changeMenu);
    return(() => {
      window.removeEventListener('resize', changeMenu);
  })
  }, [menu]);

  return (
    <header>
      <div className="header__logo">
        <h1><Link to="/"><img src={ Plane } alt="plane logo"/>Travel Agency</Link></h1>
      </div>
      <div className={ menu ? "hide" : "header__nav__burger--open" } onClick={ () => setMenu(!menu) }>
        <img src={ Menu } className="header__nav__burger--icon" alt="Menu"/>
      </div>
      <nav className={ menu ? "header__nav--open" : "header__nav" }>
        <div className="header__nav__burger--close">
          <img src={ MenuClose } className="header__nav__burger--icon" alt="Menu close" onClick={ () => setMenu(!menu) } />
        </div>
        <ul>
          <li><Link to="/" className="header__nav--active">Home</Link></li>
          <li><Link to="/destinations">Destinations</Link>
            <ul className="header__sub-nav">
              { destinations.slice(0, 3).map((destination) => {
                return (
                  <li key={ destination.id }>
                    <Link to={`/destinations/${ destination.id }`}>{ destination.title }</Link>
                  </li>
                );
              }) }
              <li><Link to="/destinations" className="header__sub-nav--see-more">See more</Link></li>
            </ul>
          </li>
          <li><Link to="/deals">Deals</Link></li>
          <li><Link to="/contact">Contact</Link></li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
