import React, { useLayoutEffect } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, useLocation } from "react-router-dom";
import { createBrowserHistory } from "history";

import App from './components/App';

import './index.css';

const Wrapper = ({children}) => {
  const location = useLocation();
  useLayoutEffect(() => {
    document.documentElement.scrollTo({ top: 0, behavior: 'smooth' });
  }, [location.pathname]);
  return children
} 

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter history={createBrowserHistory()}>
      <Wrapper>
        <App />
      </Wrapper>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);